import java.lang.reflect.Field;

public class CLIOutils {

    static public CLI fromClass(String classeNom) {

        CLI cli = new CLI();
        MyClass = Class.forName(classeNom); //renvoie l'objet Class associé à la classe "classeNom"

        for (Field champs : MyClass.getDeclaredFields()) //renvoie un tableau d'objets des champs définis dans l'objet 
        {
            Option option = new Option(champs.getName().charAt(0)/*initiale*/,champs.getName(),!champs.getType().isEnum()/*check mode*/);
            cli.addOption(option);
        }
        return cli;
    }

    //méthode main
    public static void main(String[] args) {
        CLIOutils.fromClass("Configuration");
    }

}
