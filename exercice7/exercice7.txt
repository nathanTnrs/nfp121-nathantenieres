**EXERCICE 7**

7.1)
<?XML version="1.0" encoding="UTF-8"?>

        <!ELEMENT cli (argument+)>
        <!ELEMENT argument (#PCDATA)>

        <!ATTLIST argument
                acces ID #REQUIRED
                nb CDATA #IMPLIED
	>

7.2)
<?XML version="1.0" encoding="UTF-8"?>
<!DOCTYPE cli SYSTEM "cli.dtd">

<cli>
    <argument acces="I" nb="2">Value of index</argument>
    <argument acces="A" nb="3">Value of Alpha</argument>
    <argument acces="P">Empty Matrix Mode</argument>
    <argument acces="C">Full Matrix Mode</argument>
</cli>

7.3)
Pour produire un objet CLI en Java, le fichier XML précédement exposé peut-être automatiquement généré en créant une méthode 
de séréalisation respectant le format attendu dans la DTD. 
Une fois le fichier XML généré, on créée une classe de deserialisation. Dans cette classe, on met en place une méthode de lecture 
qui à partir du fichier xml passé en paramètre va le parcourir afin d'en extaire toutes les informations.
