
import java.util.HashSet;
import java.util.Set;

public class Cli {

    private Set<Option> Loptions = new HashSet<Option>();

    public Cli(Set<Option> Loptions) {
        this.Loptions = Loptions;
    }

    public Set<Option> getOptions() {
        return Loptions;
    }

    public void setOptions(Set<Option> Loptions) {
        this.Loptions = Loptions;
    }

    public  void addOption(Option option){
        Loptions.add(option);
    }

}
