import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Assertions.assertEquals;

public class TestJUnit {

    protected static Configuration configuration;

    @BeforeAll
    static void setUp() {
        String[] arguments = "-K 10 -A .90 -K 20 -P -K 30 -C".split("\\s+");
        configuration = CLIClassique.configure(arguments);
    }

    void ModeTest() {
        assertEquals(Mode.CREUSE, configuration.getMode());
    }
    
    void AlphaTest() {
        assertEquals(0.9, configuration.getAlpha());
    }

    void EpsilonTest() {
        assertEquals(-1.0, configuration.getEpsilon());
    }

    void IndexTest() {
        assertEquals(30, configuration.getIndex());
    }
}